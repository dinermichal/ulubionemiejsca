package com.example.android.ulubionemiejsca.data;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.ulubionemiejsca.R;


public class LocationCursorAdapter extends CursorAdapter {

  public LocationCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 /* flags */);
    }


   @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView tvLocationDate = (TextView) view.findViewById(R.id.date);
        TextView tvLocationComment = (TextView) view.findViewById(R.id.comment);


        int dateColumnIndex = cursor.getColumnIndex(LocationContract.LocationEntry.COLUMN_DATE);
        int commentColumnIndex = cursor.getColumnIndex(LocationContract.LocationEntry.COLUMN_COMMENT);


        String locationDate = cursor.getString(dateColumnIndex);
        String locationComment = cursor.getString(commentColumnIndex);


        tvLocationDate.setText(locationDate);
        tvLocationComment.setText(locationComment);
    }


    }
