package com.example.android.ulubionemiejsca.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.android.ulubionemiejsca.data.LocationContract.LocationEntry;

public class LocationDbHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = LocationDbHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "location.db";


    private static final int DATABASE_VERSION = 1;

    public LocationDbHelper(Context context) { super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

      String SQL_CREATE_LOCATION_TABLE =  "create table " + LocationEntry.TABLE_NAME + " ("
                + LocationEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + LocationEntry.COLUMN_LATITUDE + " TEXT NOT NULL, "
                + LocationEntry.COLUMN_LONGITUDE + " TEXT NOT NULL, "
                + LocationEntry.COLUMN_TRANSPORT + " INTEGER NOT NULL, "
                + LocationEntry.COLUMN_DATE + " TEXT NOT NULL, "
                + LocationEntry.COLUMN_COMMENT + " TEXT );";


        db.execSQL(SQL_CREATE_LOCATION_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}