package com.example.android.ulubionemiejsca;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.ulubionemiejsca.data.LocationContract;
import com.example.android.ulubionemiejsca.data.LocationCursorAdapter;
import com.example.android.ulubionemiejsca.data.LocationDbHelper;

public class ListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {


    private static final int LOCATION_LOADER = 0;

    private LocationCursorAdapter mCursorAdapter;


    private LocationDbHelper mDbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        mDbHelper = new LocationDbHelper(this);

        ListView locationListView = (ListView) findViewById(R.id.list);

        View emptyView = findViewById(R.id.empty_view);
        locationListView.setEmptyView(emptyView);

        mCursorAdapter = new LocationCursorAdapter(this, null);
        locationListView.setAdapter(mCursorAdapter);

        locationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

           @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListActivity.this, CheckInActivity.class);
                Uri currentLocationUri = ContentUris.withAppendedId(LocationContract.LocationEntry.CONTENT_URI,id);
                intent.setData(currentLocationUri);
                startActivity(intent);
            }

            });

        getLoaderManager().initLoader(LOCATION_LOADER, null, this);
    }


    @Override
    protected void onStart() {
        super.onStart();
   }

    private void insertLocation() {

        ContentValues values = new ContentValues();
        values.put(LocationContract.LocationEntry.COLUMN_LATITUDE, "60.212");
        values.put(LocationContract.LocationEntry.COLUMN_LONGITUDE, "20.456");
        values.put(LocationContract.LocationEntry.COLUMN_TRANSPORT, 0);
        values.put(LocationContract.LocationEntry.COLUMN_DATE, "Data Example");
        values.put(LocationContract.LocationEntry.COLUMN_COMMENT, "Comment Example");

        Uri newUri = getContentResolver().insert(LocationContract.LocationEntry.CONTENT_URI, values);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()) {
             case R.id.action_insert_dummy_data:
                insertLocation();
                return true;

            case R.id.action_delete_all_entries:
                deleteAllLocations();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

     private void deleteAllLocations() {

         int rowsDeleted = getContentResolver().delete(LocationContract.LocationEntry.CONTENT_URI, null, null);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projection = {
                LocationContract.LocationEntry._ID,
                LocationContract.LocationEntry.COLUMN_DATE,
                LocationContract.LocationEntry.COLUMN_COMMENT};

        return new CursorLoader(this,
                LocationContract.LocationEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }
}


