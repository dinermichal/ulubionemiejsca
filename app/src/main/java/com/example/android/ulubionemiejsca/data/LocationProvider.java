package com.example.android.ulubionemiejsca.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import static com.example.android.ulubionemiejsca.data.LocationContract.CONTENT_AUTHORITY;
import static com.example.android.ulubionemiejsca.data.LocationContract.LocationEntry;
import static com.example.android.ulubionemiejsca.data.LocationDbHelper.LOG_TAG;

/**
 * {@link ContentProvider} for Location app.
 */
public class LocationProvider extends ContentProvider {


  private static final int LOCATION = 100;

  private static final int LOCATION_ID = 101;

  private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

  static {

       sUriMatcher.addURI(CONTENT_AUTHORITY, LocationContract.PATH_LOCATION, LOCATION);

       sUriMatcher.addURI(CONTENT_AUTHORITY, LocationContract.PATH_LOCATION + "/#", LOCATION_ID);
    }

  private LocationDbHelper mDbHelper;

    @Override
    public boolean onCreate() {

        mDbHelper = new LocationDbHelper(getContext());

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        SQLiteDatabase database = mDbHelper.getReadableDatabase();


        Cursor cursor;


        int match = sUriMatcher.match(uri);
        switch (match) {
            case LOCATION:

               cursor = database.query(LocationEntry.TABLE_NAME, projection, selection, selectionArgs
                        , null, null, sortOrder);
                break;

            case LOCATION_ID:

              selection = LocationEntry._ID + "=?";

              selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

              cursor = database.query(LocationEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);

                break;

            default:

                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }


    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case LOCATION:
                return insertLocation(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }


    private Uri insertLocation(Uri uri, ContentValues values) {

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

             long id = database.insert(LocationEntry.TABLE_NAME, null, values);
        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row for " + uri);
            return null;
        }

       getContext().getContentResolver().notifyChange(uri, null);


       return ContentUris.withAppendedId(uri, id);
    }


    @Override
    public int update(Uri uri, ContentValues contentValues, String selection,
                      String[] selectionArgs) {

        final int match = sUriMatcher.match(uri);

        switch (match) {

            case LOCATION:

                return updateLocation(uri, contentValues, selection, selectionArgs);

            case LOCATION_ID:

                selection = LocationEntry._ID + "=?";

                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                return updateLocation(uri, contentValues, selection, selectionArgs);

            default:

                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }


    private int updateLocation(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int rowsUpdated = database.update(LocationEntry.TABLE_NAME, values, selection, selectionArgs);

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        switch (match) {
            case LOCATION:
                rowsDeleted = database.delete(LocationContract.LocationEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case LOCATION_ID:
                selection = LocationContract.LocationEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = database.delete(LocationContract.LocationEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }

        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDeleted;
    }


    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case LOCATION:
                return LocationEntry.CONTENT_LIST_TYPE;
            case LOCATION_ID:
                return LocationEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }
}