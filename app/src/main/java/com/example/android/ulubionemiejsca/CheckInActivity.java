package com.example.android.ulubionemiejsca;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.ulubionemiejsca.data.LocationContract;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CheckInActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {


    private static final int EXISTING_LOCATION_LOADER = 0;

    private Uri mCurrentLocationUri;

    private TextView mLongitude;

    private TextView mLatitude;

    private TextView mDate;

    private EditText mCommentEdit;

    private Spinner mTransportSpinner;

    private Button mMapsButton;

    /**
     * Means of transport. The possible values are:
     * 0 for unknown , 1 for bike, 2 for car, 3 on foot.
     */
    private int mTransport = LocationContract.LocationEntry.TRANSPORT_UNKNOWN;

    private boolean mLocationHasChanged = false;

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mLocationHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin);


        Intent intent = getIntent();
        if (intent != null) {
            mCurrentLocationUri = intent.getData();
            if (mCurrentLocationUri == null) {
                setTitle(getString(R.string.editor_activity_title_new_location));
                invalidateOptionsMenu();
            } else {
                setTitle(getString(R.string.editor_activity_title_edit_location));
                getLoaderManager().initLoader(EXISTING_LOCATION_LOADER, null, this);
            }
        }

        mLongitude = (TextView) findViewById(R.id.edit_location_longitude);
        mLatitude = (TextView) findViewById(R.id.edit_location_latitude);
        mDate = (TextView) findViewById(R.id.edit_location_date);
        mCommentEdit = (EditText) findViewById(R.id.edit_location_comment);
        mTransportSpinner = (Spinner) findViewById(R.id.spinner_transport);
        mMapsButton = (Button) findViewById(R.id.maps_button);

        mCommentEdit.setOnTouchListener(mTouchListener);
        mTransportSpinner.setOnTouchListener(mTouchListener);

        mMapsButton.setOnClickListener(new View.OnClickListener() {

                                           GPSTracker gps = new GPSTracker((CheckInActivity.this));

                                           // The code in this method will be executed when the numbers category is clicked on.
                                           @Override
                                           public void onClick(View view) {
                                               // Create a new intent to open the {@link NumbersActivity}
                                               if (gps.canGetLocation()) {

                                                   double latitude = gps.getLatitude();
                                                   double longitude = gps.getLongitude();
                                                   String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
                                                   Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));

                                                   // Start the new activity
                                                   startActivity(intent);
                                               }else {
                                                   // can't get location
                                                   // GPS or Network is not enabled
                                                   // Ask user to enable GPS/network in settings
                                                   gps.showSettingsAlert();}

                                           }
                                       });


        setupSpinner();
        showAdditionalInfo();
        time();

    }


    private void time (){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        mDate.setText(formattedDate);
    }
    public void showAdditionalInfo() {
        // create class object
        GPSTracker gps = new GPSTracker(CheckInActivity.this);
        // check if GPS enabled
        if (gps.canGetLocation()) {



            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            String latitudeStr = String.valueOf(latitude);
            String longitudeStr = String.valueOf(longitude);
            final String lon = "Longitude: " + longitudeStr;
            final String lat = "Latitude: " + latitudeStr;
            mLatitude.setText(lat);
            mLongitude.setText(lon);
           } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

    }


    private void setupSpinner() {

        ArrayAdapter transportSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_transport_options, android.R.layout.simple_spinner_item);

         transportSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        mTransportSpinner.setAdapter(transportSpinnerAdapter);

        mTransportSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.transport_bike))) {
                        mTransport = LocationContract.LocationEntry.TRANSPORT_BIKE; // Bike
                    } else if (selection.equals(getString(R.string.transport_car))) {
                        mTransport = LocationContract.LocationEntry.TRANSPORT_CAR; // Car
                    } else if (selection.equals(getString(R.string.transport_onfoot))) {
                        mTransport = LocationContract.LocationEntry.TRANSPORT_ONFOOT; // On Foot
                    } else {
                        mTransport = LocationContract.LocationEntry.TRANSPORT_UNKNOWN; // Unknown
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {mTransport =
                    LocationContract.LocationEntry.TRANSPORT_UNKNOWN; // Unknown
            }
        });
    }


    private void setLocation() {

        String latitudeString = mLatitude.getText().toString().trim();
        String longitudeString = mLongitude.getText().toString().trim();
        String dateString = mDate.getText().toString().trim();
        String commentString = mCommentEdit.getText().toString().trim();


        ContentValues values = new ContentValues();
        values.put(LocationContract.LocationEntry.COLUMN_LATITUDE, latitudeString);
        values.put(LocationContract.LocationEntry.COLUMN_LONGITUDE, longitudeString);
        values.put(LocationContract.LocationEntry.COLUMN_TRANSPORT, mTransport);
        values.put(LocationContract.LocationEntry.COLUMN_DATE, dateString);
        values.put(LocationContract.LocationEntry.COLUMN_COMMENT, commentString);

        if (mCurrentLocationUri == null) {
            Uri uri = getContentResolver().insert(LocationContract.LocationEntry.CONTENT_URI, values);
            if (uri == null) {
                Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.location_save), Toast.LENGTH_SHORT).show();
            }
        } else {
            int rowsAffected = getContentResolver().update(mCurrentLocationUri, values, null, null);
            if (rowsAffected == 0) {
                Toast.makeText(this, getString(R.string.error),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.location_save),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_checkin, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mCurrentLocationUri == null) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
               setLocation();
                finish();
                return true;
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
            case R.id.action_change_id:
                showChangingRowDialog();
                return true;
            case android.R.id.home:
                if (!mLocationHasChanged) {
                    NavUtils.navigateUpFromSameTask(CheckInActivity.this);
                    return true;
                }
                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                NavUtils.navigateUpFromSameTask(CheckInActivity.this);
                            }
                        };
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showUnsavedChangesDialog(DialogInterface.OnClickListener discardButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        if (!mLocationHasChanged) {
            super.onBackPressed();
            return;
        }
        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                };
        showUnsavedChangesDialog(discardButtonClickListener);
    }

    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteLocation();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteLocation() {
        if (mCurrentLocationUri != null) {
            int rowsDeleted = getContentResolver().delete(mCurrentLocationUri, null, null);
            if (rowsDeleted == 0) {
                Toast.makeText(this, getString(R.string.editor_delete_location_failed), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.editor_delete_location_successful), Toast.LENGTH_SHORT).show();
            }
            finish();
        }
    }

    private void showChangingRowDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText text = new EditText(this);

        builder.setTitle("").setMessage(R.string.change_row_dialog_msg).setView(text);
        builder.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface di, int i) {
                final int newId = Integer.parseInt(text.getText().toString());
                ContentValues values = new ContentValues();
                values.put(LocationContract.LocationEntry._ID, newId);
                Uri uri = getContentResolver().insert(LocationContract.LocationEntry.CONTENT_URI, values);
                // Unfortunetly this doesn't work despite my efforts, sorry DroidsOnRoids :c
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface di, int i) {
            }
        });
        builder.create().show();


    }



    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {

        String[] projection = {
                LocationContract.LocationEntry._ID,
                LocationContract.LocationEntry.COLUMN_LATITUDE,
                LocationContract.LocationEntry.COLUMN_LONGITUDE,
                LocationContract.LocationEntry.COLUMN_TRANSPORT,
                LocationContract.LocationEntry.COLUMN_DATE,
                LocationContract.LocationEntry.COLUMN_COMMENT};

        return new CursorLoader(this,
                mCurrentLocationUri,
                projection,
                null,
                null,
                null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            if (data.moveToFirst()) {
                // Find the columns of location attributes that we're interested in
                int latitudeColumnIndex = data.getColumnIndex(LocationContract.LocationEntry.COLUMN_LATITUDE);
                int longitudeColumnIndex = data.getColumnIndex(LocationContract.LocationEntry.COLUMN_LONGITUDE);
                int transportColumnIndex = data.getColumnIndex(LocationContract.LocationEntry.COLUMN_TRANSPORT);
                int dateColumnIndex = data.getColumnIndex(LocationContract.LocationEntry.COLUMN_DATE);
                int commentColumnIndex = data.getColumnIndex(LocationContract.LocationEntry.COLUMN_COMMENT);

                String latitude = data.getString(latitudeColumnIndex);
                String longitude = data.getString(longitudeColumnIndex);
                int transport = data.getInt(transportColumnIndex);
                String date = data.getString(dateColumnIndex);
                String comment = data.getString(commentColumnIndex);

                mLatitude.setText(latitude);
                mLongitude.setText(longitude);
                mDate.setText(date);
                mCommentEdit.setText(comment);
                switch (transport) {
                    case LocationContract.LocationEntry.TRANSPORT_BIKE:
                        mTransportSpinner.setSelection(1);
                        break;
                    case LocationContract.LocationEntry.TRANSPORT_CAR:
                        mTransportSpinner.setSelection(2);
                        break;
                    case LocationContract.LocationEntry.TRANSPORT_ONFOOT:
                        mTransportSpinner.setSelection(3);
                        break;
                    default:
                        mTransportSpinner.setSelection(0);
                        break;
                }
            }
        }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mLatitude.setText("");
        mLongitude.setText("");
        mDate.setText("");
        mCommentEdit.setText("");
        mTransportSpinner.setSelection(0);
    }
}


