package com.example.android.ulubionemiejsca;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

public class MainActivity extends Activity {

    private TextView tvProvider;
    private TextView tvLongitude;
    private TextView tvLatitude;
    private TextView tvInformations;
    private Button checkIn;
    private Button listOfPlaces;
    private LocationManager locationManager;
    private Location savedLocation = null;
    private LocationListener locationListener = new LocationListener()


    {

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }

        public void onLocationChanged(Location location) {
            showLocation(location);
            showAdditionalInfo(location);
            savedLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }


    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvProvider = (TextView) findViewById(R.id.tvProvider);
        tvLatitude = (TextView) findViewById(R.id.tvLatitude);
        tvLongitude = (TextView) findViewById(R.id.tvLongitude);

        tvInformations = (TextView) findViewById(R.id.tvInformations);
        checkIn = (Button) findViewById(R.id.save_to_list);
        listOfPlaces = (Button) findViewById(R.id.list_of_places);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            tvProvider.setText("GPS");
        else
            tvProvider.setText("GPS Disabled. Please, turn it on");


       listOfPlaces.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listIntent = new Intent(MainActivity.this, ListActivity.class);

                startActivity(listIntent);
            }
        });

        checkIn.setOnClickListener(new OnClickListener() {

            @Override
           public void onClick(View v) {

                Intent listIntent = new Intent(MainActivity.this, CheckInActivity.class);

                startActivity(listIntent);
            }
        });




    }
    @Override
    protected void onStart() {
        super.onStart();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
              return;
        }
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                3000, 2, locationListener);
    }

    @Override
    protected void onStop() {
        locationManager.removeUpdates(locationListener);
        super.onStop();
    }

    public void showLocation(Location location) {
        String latitude = "Latitude: ";
        String longitude = "Longitude: ";
        if (location != null) {
            latitude += location.getLatitude();
            longitude += location.getLongitude();
            tvLatitude.setText(latitude);
            tvLongitude.setText(longitude);
        }
    }



    private void showAdditionalInfo(Location location) {
        String infos = "Distance from first fix: ";
        if (savedLocation == null || location == null) {
            infos += "can't calculate";
        } else {
            infos += savedLocation.distanceTo(location) + "m\n";
            infos += "Accuracy: ";
            infos += location.getAccuracy() + "m \n";
            infos += "Last fix: ";
            infos += new Date(location.getTime()).toGMTString() + "\n";
            infos += "Speed: ";
            infos += location.getSpeed() + "m/s";
        }
        tvInformations.setText(infos);
    }
}