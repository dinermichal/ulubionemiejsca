package com.example.android.ulubionemiejsca.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class LocationContract {

    private LocationContract() {}

    public static final String CONTENT_AUTHORITY = "com.example.android.ulubionemiejsca";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_LOCATION = "location";

    public static final class LocationEntry implements BaseColumns {

    public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_LOCATION);

    public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;

    public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;

    public final static String TABLE_NAME = "location";

    public final static String _ID = BaseColumns._ID;

    public final static String COLUMN_LATITUDE ="latitude";

    public final static String COLUMN_LONGITUDE = "longitude";

        /**
         * Means of transport of the location.
         *
         * The only possible values are {@link #TRANSPORT_UNKNOWN}, {@link #TRANSPORT_BIKE}, {@link #TRANSPORT_CAR}
         * or {@link #TRANSPORT_ONFOOT}.
         *
         * Type: INTEGER
         */
    public final static String COLUMN_TRANSPORT = "transport";

    public final static String COLUMN_DATE = "date";

    public final static String COLUMN_COMMENT = "comment";

    public static boolean isValidTransport(int transport) {

        return transport == TRANSPORT_UNKNOWN || transport == TRANSPORT_BIKE || transport == TRANSPORT_CAR
                    || transport == TRANSPORT_ONFOOT;
        }

        public static final int TRANSPORT_UNKNOWN = 0;
        public static final int TRANSPORT_BIKE = 1;
        public static final int TRANSPORT_CAR = 2;
        public static final int TRANSPORT_ONFOOT = 3;

    }

}